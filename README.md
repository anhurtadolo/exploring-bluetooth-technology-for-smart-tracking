# Documentation of Design Driven Project Group A: Exploring Bluetooth Technology for Smart Tracking
By Yuan Yao Lum and Andres Hurtado Lopez

![Final UI](media/implementation_video_2x.gif)

In this README file, the entire exploration on Bluetooth and Bluetooth Low Energy technology as well as how we can utilize it for construction site analytics is documented. Smart Tracking tehnology is a hard to implement as it comes with many different challenges in the construction site. Therefore as a student of the master course Construction Robotics in RWTH Aachen, we would like to find a way to make this happen. This Design Driven Project was done in the summer semester of 2021. 
## Main Project
In this main project, with the help of my teammate Andres Hurtado Lopez and our supervisor Orwa Diraneyya, we were able to build a functional Bluetooth Device Scanner using:
* Raspberry Pi 4(s)
* Open-source repository [monitor.sh](https://github.com/andrewjfreyer/monitor)
* Visual programming tool Node-RED.  

We started looking for different beacons to experiment with and different mobile applications with which we could identify the signal and the different characteristics and information provided by the scanner. 

That is why we decided to acquire a beacon and download different applications of which we have the following: 

acquired beacon: 

FEASYCOM Waterproof BLE iBeacon Bluetooth 5.1

![beacon](media/beacon.jpg)

Chosen bluetooth application: 

nRF Connect

![bluetooth scanner](media/nfr.gif)


From there it was easier to identify that we needed a tool in which we could identify as many devices as possible through bluetooth technology regardless of whether it was BLE or normal bluetooth. This is how we found the monitor script.

To summarize how this works, the _monitor_ script is run on the Raspberry Pi(s) to scan for Bluetooth devices in its vicinity. These messages are then published using a MQTT broker that is also run locally in the Raspberry Pi. We then use Node-RED to receive these MQTT messages and store them in a SQL database. Then, a User Interface is built to show the devices that were detected by the _monitor_ script and they are categorized by their location. For example, if the devices are detected on Raspberry Pi A, they will show up in the “Zone A” column of the User Interface (as shown in the GIF above). The detailed documentation of the work is shown below.

### _monitor_ script 
[Monitor.sh](https://github.com/andrewjfreyer/monitor) is an open source shell script that scans all bluetooth devices in the surroundings. It is intended to run on a Raspberry Pi Zero for home automation. Follow the following steps to clone the _monitor_ repository.    

Configuration and Setup:
1. Upgrade and update all available packages in Raspberry Pi with the following commands:
    ```
    $ sudo apt-get update
    $ sudo apt-get upgrade -y
    $ sudo apt-get dist-upgrade -y
    $ sudo reboot
    ```
2. Install Bluetooth firmware using this command:  
`$ sudo apt-get install pi-bluetooth`
3. Restart Raspberry Pi:    
`$ sudo reboot`
4. Install Mosquitto version 1.5+ 
    ```
    #Get the repo key:
    $ wget http://repo.mosquitto.org/debian/mosquitto-repo.gpg.key
    #Add the repo:
    $ sudo apt-key add mosquitto-repo.gpg.key
    #Download appropriate lists file 
    $ cd /etc/apt/sources.list.d/
    $ sudo wget http://repo.mosquitto.org/debian/mosquitto-buster.list
    #Update caches and install mosquitto
    $ apt-cache search mosquitto
    $ sudo apt-get update
    $ sudo apt-get install -f libmosquitto-dev mosquitto mosquitto-clients libmosquitto1
    ```
5. Clone _monitor_ git. Locate to the folder that you want to clone the repository using the `$ cd` command. Then continue with the following commands.
    ```
    #Install git:
    $ sudo apt-get install git
    #Clone the repo:
    $ git clone git://github.com/andrewjfreyer/monitor
    #Enter the `monitor` directory:
    $ cd monitor/
    ```
#### _installation_ _attempt_ _on_ _android-based_ _mobile phone_: 
Through the application termux which can be found in the play store it was installed and from there it was possible to run node red on the mobile phone.

![termux](media/termuxinstallation.jpeg)

we installed node-red using the command prompt in termux as you can find here in this link 

https://wiki.instar.com/en/Advanced_User/Node-RED_on_Android/

Then, in order to be able to visualise the monitor script on the mobile phone, we proceeded to perform the previous steps, but this was not possible because the mobile phone system must be rooted in order to be able to execute the linux-based commands and install the necessary packages.

#### Edit preferences:

1. Before running the _monitor_ script, it would be nice to edit the `mqtt_preferences` file first. Open the `mqtt_preferences` file in the _monitor_ repo folder. Change the MQTT broker username and password. You can also set the MQTT topic path here. 
<!-- ![mqtt preferences file](media/mqtt_preferences_group.png) -->
<img src="media/mqtt_preferences_group.png"  width="360">

2. To constantly get a read on a specific Bluetooth device, you can also add the MAC address of the device to the `known_static_addresses` file, i.e. `A1:B2:C3:D4:E5:F6 iPhone `. This way, the _monitor_ script will constantly scan this device.      
![known static addresses](media/known_static_addresses.jpg)
3.	For Bluetooth Low Energy (BLE) Beacons, the same changes could be done to the `known_beacon_addresses` file.
![known beacon addresses](media/known_beacon_addresses.jpg)

Finally, you are now able to run the _monitor_ script! In the terminal, run the following command:    
` $ sudo bash monitor.sh`  
To scan for BLE beacons add -b to the command:    
` $ sudo bash monitor.sh -b`   
All Bluetooth devices in the vicinity should now appear in the terminal log. 
![monitor.sh](media/monitor_sh_crop_censor.jpg)
Now, to output the MQTT messages in a readable manner, Node-RED is used to receive the MQTT messages by subscribing to the topic sent by the _monitor_ script and show it in a user-friendly interface.

### Node-RED
Node-RED is a visual programming tool that allows information from the real world to be taken in, then using differnt nodes with specific functions to change the data (could be done without having any programming knowledge), and finally output it to the real world. All of the Raspberry Pi’s provided from the chair of Construction Robotics have Node-RED installed, but if you need to install Node-RED, please check out this guide: [Node-RED installation guide](https://nodered.org/docs/getting-started/local). Run Node-RED locally by typing in the command in the terminal: `$ node-red`. Now you can access Node-RED with a web browser with the url: http://localhost:1880.
![Node-RED](media/start_node_red.png)  

#### Receive MQTT messages in Node-RED
MQT is essentially a subscription based messaging system. To receive the messages, you would need to subscribe to a specific toppic, and you will be able to listen to all messages under this topic. To take in MQTT messages from the _monitor_ script in Node-RED, look for the mqtt in node in the node palette found on the left. Drag it and deploy the _mqtt in_ node.
<!-- ![mqtt nodes](media/mqtt_nodes_word.png) -->
<img src="media/mqtt_nodes_word.png"  width="180"> 

#### Configure the MQTT broker
1.	Double click the _mqtt in_ node to configure the settings.
2.	Press the pencil icon to configure the MQTT broker.
3.	As the Mosquitto is also running locally on the Raspberry Pi, the server is set as 127.0.0.1. The default port is left at 1883.
4.	Press update.
5.	To obtain the MQTT messages from _monitor_ script, the topic is set to monitor/#, as the default main topic is monitor. You can change the topic in the `mqtt_preferences` file in the _monitor_ repo folder.  # includes all of the subtopics under the main topic monitor.
<!--![mqtt in](media/mqtt_in_word.png)
![mqtt broker](media/mqtt_broker_word.png) -->
<img src="media/mqtt_in_word.png"  width="360">
<img src="media/mqtt_broker_word.png"  width="360">

To output the MQTT messages, drag and deploy a _debug_ node connected to the _mqtt in_ node. Messages should be appearing on the debug window. Now that we are able to receive MQTT messages in Node-RED, the next step is to deliver the messages into a SQL database.

#### SQL Database (SQLite and influxDB) using Node-RED
To get a consistent update on the “where and when” of the Bluetooth devices, it makes sense to establish a SQL database. In this prototype, 2 types of SQL database were generated, which are SQLite and influxDB, the only difference being the latter with an additional automatically generated timestamp column for each entry. The influxDB database is discussed in the [influxDB](#5-influxdb) section of [Other approaches](#other-approaches).  SQLite and influxDB are not found in the pre-existing modules in Node-RED. The modules required for the prototype are listed as follow:  
•	`node-red-node-sqlite `  
•	`node-red-contrib-influxdb` (in [influxDB](#5-influxdb) )  
•	`node-red-contrib-spreadsheet-in`

#### Installing new modules in Node-RED:
1.	To install additional modules on Node-RED, first click on the button on the top right corner of the Node-RED interface.
2.	Click “Manage palette”.
3.	Press the “Install” tab.
4.	Search for the module. In this example, it’s the node-red-node-sqlite module. 
5.	Click the install button.

<!-- ![install new module](media/manage_palette_word.png)
![install sqlite module](media/install_sqlite.png)  -->
<img src="media/manage_palette_word.png"  width="280">
<img src="media/install_sqlite.png"  width="480">



#### Configuring SQLite Table
After successfully installing the sqlite module, look for the _sqlite_ node in the node palette on the left. 
1.	Drag and deploy the _sqlite_ node.
2.	Set a name for the _sqlite_ node.
3.	Press the pencil icon.
4.	Set the path where you want to place the SQLite database. In Raspberry Pi, the default path is tmp/sqlite.
5.	Click “Update”.
6.	Pay attention that the SQLite database receives SQL query through the msg.topic.

#### Initializing SQLite Table
![sqlite initialization](media/sqlite_initialization.png)

To initialize a table in the SQLite database, an online SQLite query generator was used to create a table called “bluetooth_devices”. This table takes the MAC address of each Bluetooth device as the primary key (unique identifier) and takes in the "name", "manufacturer", "location", "timestamp", "rssi" and "confidence" of each entry. The following query was used using an inject node and by setting the msg. topic to: 
```
CREATE TABLE bluetooth_devices (id char(32) NOT NULL PRIMARY KEY, name varchar(255) NOT NULL, manufacturer varchar(255) NOT NULL, location varchar(255) NOT NULL, timestamp varchar(255) NOT NULL, rssi varchar(255) NOT NULL,confidence varchar(255) NOT NULL);
```

#### Pre-processing of MQTT messages
Before entering the raw MQTT messages into the database, it would be wise to do some pre-processing and filter out some noise.

<!-- ![pre-processing mqtt messages](media/pre_processing_mqtt_word.png) -->
<img src="media/pre_processing_mqtt_word.png"  width="480"> 
 
From the raw MQTT message, the default topic would be in the following manner: `monitor/Raspberry Pi A/MAC Address of detected Bluetooth device/rssi (if available)`. The first subtopic shows which Raspberry Pi detected the Bluetooth device, the second subtopic is the MAC address of the detected device, and if available the third subtopic gives the rssi strength of the device. To extract the location of the detected device, the first subtopic, in this case, (Raspberry Pi) A was used as the “location” of the device.

1.	First, the topic is split into different topic part using a _switch_ node.
2.	Using a _change_ node, the rssi subtopic is filtered out.
3.	The status of the _monitor_ script is also filtered out using a _change_ node.
4.	With the help of a _json_ parser node, the string message is parsed into an Object.
5.	With another _change_ node, the first subtopic of the message is added into the msg.payload with the title “location”.
6.	Finally, to filter noise that have random MAC addresses, a _change_ node is used to filter out MAC addresses that do not have 17 characters.
7.	Then, using a _template_ node, the msg.payload is entered into the database using the REPLACE query.

*Note: Please refer to the [.json](core/ble_yuan_final.json) file attached to look at the settings of each individual nodes.*

To ensure a User Interface that is simple and easy to understand, a new table “device_location” is created that has the MAC address of the detected Bluetooth device as primary key, and the “location” and “rssi_or_confidence” of the device as alternate keys. This would give us the information of where the detected device is at that moment in time. Using an _inject_ node as shown before, the “device_location” table is initialized.  

#### Implement requirements to update database
Up till this point, the flow works fine when there is only 1 Raspberry Pi deployed. When there are 2 different Raspberry Pi running the _monitor_ script, there would be 2 different MQTT messages as input from 2 different Raspberry Pi for each device into the database. To prevent complications, some logic should be implemented.   
•	Check if the "location" of the device has changed.  
•	If location has changed, the "rssi or confidence" value has to be greater than the old value, in order to replace the existing entry in the database. In this case, the Raspberry Pi that is the closest to the device is taken as the “location” of the device.  

![requirements to update database](media/requirements_word.png) 

1.	From the change node that filters noise, a _function_ node is connected to it to introduce a new alternate key of “rssi_or_confidence”. If "rssi" is available, it will take the "rssi" value; if the "rssi" is unavailable, the "confidence" value is taken instead.
2.	With the _template_ node, a query statement is made to the database based on the values of the Object in the msg.payload from the _monitor_ script. 
3.	Before querying, the msg.payload is backtracked and saved into a new msg property called msg.payload_before to prevent the new msg.payload from overwriting the previous msg.payload.
4.	After querying the database, using a _change_ node, the new msg.payload is changed into msg.query_result whereas the msg.payload_before is revert back into msg.payload to prevent confusion. 
5.	A _switch_ node is used to check if there is existing entry of the detected device in the database. If the query result is empty, the msg.payload is updated into the database. Otherwise, it continues to the next step.
6.	A second _switch_ node is used to check if the “location” of the device has changed. If it has not changed, the msg.payload is updated into the database. Otherwise, it continues to the next node.
7.	The last _switch_ node then checks if the “rssi_or_confidence” value is greater than the previous value. If it is greater, the msg.payload is updated into the database. Otherwise, the message is ignored.  

*Note: Please refer to the [.json](core/ble_yuan_final.json) file attached to look at the settings of each individual nodes.*

### User Interface
With the valuable information in the SQLite database, it would be nice to have a user-friendly User Interface to output this data in a clear and simple manner. In the SQLite database, only the “id” (MAC address), “location” and “rssi_or_confidence” value of each detected Bluetooth device is stored. Attaching a photo corresponding to the device would give a clear picture to the user what kind of device was identified. Thus, an excel file was created with the “id” of the devices that were identified in the surrounding beforehand with their corresponding image addresses from Google.  
![excel list](media/excel_image_list_censor.jpg)  

#### Importing excel file into Node-RED
With the excel file, images can now be output on to the User Interface as an icon. Node-RED has an additional module called node-red-contrib-spreadsheet-in that allows us to import an excel file in Node-RED. If you followed all the instructions above and installed all the necessary modules, please continue ahead. If not, please follow the [instructions above](#installing-new-modules-in-node-red) to install the module.

<!-- ![import excel list](media/import_excel_word.png)
![file in node](media/file_in_word.png)
![sheet to json node](media/sheet_to_json_word.png) -->
<img src="media/import_excel_word.png"  width="480"> 
<img src="media/file_in_word.png"  width="360">
<img src="media/sheet_to_json_word.png"  width="360">  

1.	Drag and place a _file in_ node.
2.	Double click the _file in_ node.
3.	Set the path of the excel file in the “Filename” column.
4.	Change the output to “a single Buffer object”.
5.	Change the name of the node.
6.	Drag and connect the _book_ node to convert the excel file into a workbook object.
7.	Drag and connect the _sheet_ node. This allows you to select which sheet page your table is in the excel sheet. In this case, the “Sheet name” is “Page 1”.
8.	Finally, drag and connect the _sheet-to-json_ node to specify the range of cells of your workbook. i.e. `A1:B12`.

#### Adding the images to the detected device
If the detected device has a matching “id” with the excel workbook, the image address of the detected device is added into the Object with the key “icon”. 
![add excel list](media/add_excel_list_word.png) 
1.	First, an _inject_ or a _button_ node is used to query all of the devices in the SQLite database.
2.	Then, with a _switch_ node, if there is no entry in the database, it will set the “id” of the message to “No device found”. More information is available in the next section [Filter empty arrays](#filter-empty-arrays). Otherwise, it continues to the next node.

<!-- ![join node](media/join_node_word.png) -->
<img src="media/join_node_word.png"  width="480">  

3.	Using a _join_ node, the excel workbook from the previous section is added as msg.payload on top of the existing msg.payload from the database. Now, the msg.payload itself is an array that contains 2 arrays inside (1. array of devices detected, 2. array of images). Configure the join node settings by changing the “Mode” to “manual”. Set the “Combine each” part to “msg.payload” and “to create” to “an Array”. Then, type in “2” for the “count” of the message parts. This ensures that the message is sent only after both messages from the database and excel file are received.  
![import excel list](media/function_insert_image_links.png) 
4.	As the sequence of message received is different, a _function_ node is used to differentiate the array of image from the excel workbook from the array of detected object from the database. Since the length of array of image is constant (in this case 12), the function loops through the “id”s in the image array to compare the “id”s from the array from the database. If a match is found, a new key “icon” is added to the element of the corresponding object in the array of the database.
![delete excel payload](media/function_delete_excel_payload.png)  
5.	After that, the array of image is deleted and the array of database is now set as the msg.payload.
![check if in range](media/function_in_range.png) 
6.	Next, each detected device based on the “rssi_or_confidence” value are seperated using another _function_ node. If the “rssi_or_confidence” value is below a certain threshold (weak rssi ≤ -80 or weak confidence ≤ 20), it will be placed into a new array called “Out of range”. If the “rssi_or_confidence” value is above the threshold, it is placed into the array “In range”.
![filter based on location](media/function_filter_location.png)  
7.	 Next step is to filter the devices based on their “location”. With the help of yet another _function_ node, the devices in the array of “In range” are separated based on its “location”. In this setup, there were 2 Raspberry Pi deployed with the name A and B. Thus, the devices that have “location:  A” are placed in a new array called “A” in msg.location. Likewise, devices in “location: B” are placed in a new array “B” in msg.location.

#### Filter empty arrays
![filter empty array](media/filter_empty_array_word.png)  
If the array is empty, the UI would not refresh or output a new list. Therefore, the non-empty array is first filtered using _switch_ nodes and output to the User Interface. If it is empty, a dummy array with the “id: No device found” is output to the User Interface using a _change_ node.  

*Note: Please refer to the [.json](core/ble_yuan_final.json) file attached to look at the settings of each individual nodes.*

#### Template Node
![ui template node](media/ui_template_node.png)  
Finally for the User Interface, a template node using HTML is constructed. In the image above, this will output a list of detected device in location A with its “id” and “icon”.  If no image could be found from the excel workbook, “No Image” is printed.  The User Interface is shown below.

**For detailed information about the entire flow, please check the [.json](core/ble_yuan_final.json) file in the folder "core" for the configuration of the nodes.**

![final UI](media/ui_crop.png) 

### Future upgrades
This Bluetooth-based logistic system is currently run on a Raspberry Pi with a local SQL database. If this setup is to run on a cloud hosted system with a shared SQL database, different Raspberry Pi could be placed in different locations in a construction site to access and update the same cloud-based SQL database.

## Other approaches
Here, different approaches that were although not used in the main project, but still worth mentioning are discussed. In the beginning, some Node-RED Libraries for Bluetooth Low Energy Scanner were explored. 2 different Node-RED Libraries and a bluetooth module hardware were tested. Then, after downloading a phone app called nRFConnect, we were able to detect all kinds of bluetooh devices in the surroundings, which made us think that a general bluetooth scanenr would give us much more information.

### 1. Installing node-red-contrib-beacon-scanner
Source: [node-red-contrib-beacon-scanner]( https://flows.nodered.org/node/node-red-contrib-beacon-scanner ) 
Prerequisites:
• abandonware/noble [A Node.js BLE Module). To install this module:
1. Check kernel version to be 3.6 or above (tested with Linux 5.4.83-v7l+ armv7l) using this command:  
`$ uname -srm`
2.	Install the libbluetooth-dev library with the following command:   
`$ sudo apt-get install bluetooth bluez libbluetooth-dev libudev-dev`
3.	Then install the BLE module with:  
` $ npm install @abandonware/noble`
4.	Lastly, run this command:
```
$ sudo setcap cap_net_raw+eip $(eval readlink -f `which node`)
```
• Node.js 6 + (tested with Node.js v12.20.1). Download it here: [Node.js](https://nodejs.org/en/download/). To check Node.js version, use this command:  
`$ node –version`
Finally, install the beacon-scanner node on Node-RED or by running this command in the terminal:  
`$ npm install node-red-contrib-beacon-scanner`

For more information, refer to these links:  
•	[futomi github]( https://github.com/futomi/node-beacon-scanner#beaconscanneradvertisement-object )  
•	[abandonware/noble](https://github.com/abandonware/noble)
#### Results
We were only able to detect BLE signals of our beacon as well as other BLE devices such as Bose Earphones (very limited). No Bluetooth devices were found.

### 2. Installing node-red-contrib-noble-bluetooth
Source: [node-red-contrib-noble-bluetooth]( https://flows.nodered.org/node/node-red-contrib-noble-bluetooth ) 
Prerequisites
• abandonware/noble. Refer to [instruction above](#1-installing-node-red-contrib-beacon-scanner) on installing `node-red-contrib-beacon-scanner`.
Then, install the noble-bluetooth node on Node-RED or by running this command in the terminal:  
`$ npm install node-red-contrib-noble-scanner`
#### Results
This module was only able to detect 1 device per scan, sometimes obtaining duplicates (does not take up Windows advertisement nor HILTI BLE Tags).

### 3. Zs-040 Bluetooth module and USB-to-TTL converter
After trying these different software solutions, we turned to a hardware solution and attempted to use a Zs-040 Bluetooth module with a USB-to-TTL converter.
 
<!-- ![Zs-040 Bluetooth module](media/zs_040.jpg) -->
<!-- ![USB-to-TTL converter](media/usb_to_ttl.jpg)  -->
<img src="media/zs_040.jpg"  width="240">
<img src="media/usb_to_ttl.jpg"  width="240">

Zs-040 Bluetooth Module // USB-to-TTL converter  
A detailed guide on setting up the Bluetooth module with the USB-to-TTL converter can be found here: [Bluetooth module guide](http://www.martyncurrey.com/arduino-with-hc-05-bluetooth-module-at-mode/).   
#### Results
Here, we were able to connect the Bluetooth module to a laptop using this USB-to-TTL converter. By enabling the EN pin of the Bluetooth module, full AT commands could be sent to the Bluetooth module with the laptop. Using AT commands, master and slave relation could be set. This Bluetooth module however only allows pairing between 2 devices. It is most commonly used with Arduino’s that has no Bluetooth adapter. 

__At this point, we continued towards the main project until it is finished. The following 2 approaches were done to explore different ways to visualize our results.__

### 4.1 UI plan view
![Plan UI](media/ui_plan.png) 
Instead of using a table or list view, another approach was to show the detected Bluetooth devices in different zone in a plan view. Here a new Node-RED module is installed `node-red-contrib-ui-svg`.
First, the user enters the name of the device to search for. The name of the device is then used to query from the database. A _switch_ node is then used to separate the detected device into different zone. If the device is found in Zone A, the “suitcase” icon will turn “green” in Zone A. If the device is not found, a notification “Not Found” will be activated.  

_Note: The flow (disabled) is also found in the [.json](core/ble_yuan_final.json) file attached in the folder "core"._

### 4.1 Attemp UI Map view

![Map UI](media/redmap.png) 
Instead of using a table or list view, another approach was to show the detected Bluetooth devices in different zone in a plan view. Here a new Node-RED module is installed `node-red-contrib-web-worldmap`.
Work was also undertaken to use this node but it was not possible to connect the coordinates displayed in the database with the visualisation. Work on the project is currently ongoing. 

### 4.2 Attemp UI X & Y AXIS

![xygif](media/xygif.gif) 

 The database was manipulated in order to enter x and y coordinates that could be visualised by means of a network node flow.

 ![xygif](media/xy_2.png) 

 ### 4.2 UI list

![xygif_3](media/list_1.PNG) 

 The list node was used to capture from the sqlite database and display the devices according to the zone on the application screen.

 ![xygif_4](media/list2.PNG) 





### 5. influxDB
After venturing into SQLite, a new idea to use an influxDB database with auto-generated timestamp could be able to help our cause.
![adapt influxdb](media/adapt_influxdb.png)   
First,  the string value types of the “rssi” and “confidence” from the MQTT messages sent by the _monitor_ script are converted into numbers. Then using a _change_ node, the MQTT messages were changed into the required format to enter the influxDB database. The database is then output in the terminal log as shown below.
![influxdb database](media/influxdb_crop_censor.jpg)  
Then, using an online visualization tool called [Grafana](https://grafana.com/grafana/dashboards), the “rssi” and “confidence” values of each detected device can be shown on a time series graph dashboard using simple influxQL queries. Using Grafana, the data can be easily visualized based on the timeframe, filtered using different keys or identifier as well as using different colours to show different devices. In the photo below, the first graph shows the "rssi" value of the BLE beacons whereas the second graph shows the "confidence" value of all Bluetooth devices in the influxDB database.
![grafana ui](media/grafana_rssi_confidence_crop.png) 
 
 ## Conclusion
This project was really fruitful for me as I got to learn to use different visualization tools together with an open-source library to build a functional Bluetooth-based logistic system with a SQL database. There are many enhancements that could be done, such as a showing the position of the Bluetooth devices in a real-time plan view map with X-Y coordinates using triangulation to estimate the position of the devices or a notifation system to inform workers that if a device is out of bounds or has not been used for a long time. There are many possibilites to continue this project and I hope it could be of use to you.   



